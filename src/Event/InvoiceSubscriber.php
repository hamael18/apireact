<?php


namespace App\Event;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\Invoice;
use App\Repository\InvoiceRepository;
use DateTime;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Security;

/**
 * Class InvoiceChronoSubscriber
 *
 * @author Julie
 */
class InvoiceSubscriber implements EventSubscriberInterface
{
    private $security;

    private $invoiceRepository;

    public function __construct(Security $security, InvoiceRepository $invoiceRepository)
    {
        $this->security = $security;
        $this->invoiceRepository = $invoiceRepository;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['setDataInvoice', EventPriorities::PRE_VALIDATE]
        ];
    }

    public function setDataInvoice(GetResponseForControllerResultEvent $event)
    {
        $invoice = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if ($invoice instanceof Invoice && $method === "POST")
        {
            $this->setChrono($invoice);
            $this->setSentAt($invoice);
        }
    }

    public function setChrono(Invoice $invoice)
    {
        $nextChrono = $this->invoiceRepository->findNextChrono($this->security->getUser());
        $invoice->setChrono($nextChrono);
    }

    public function setSentAt(Invoice $invoice)
    {
        if(empty($invoice->getSentAt())){
            $invoice->setSentAt(new DateTime());
        }
    }

}