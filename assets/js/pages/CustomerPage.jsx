import React, {useEffect, useState} from 'react';
import Field from "../components/forms/Fields";
import {Link} from "react-router-dom";
import Axios from "axios";

Axios;

const CustomerPage = (props) => {

    const [customer, setCustomer] = useState({
        lastName: "",
        firstName: "",
        email: "",
        company: ""

    });

    const handleChange = ({currentTarget}) => {
        const {name, value} = currentTarget;
        setCustomer({...customer, [name]: value});
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        try {
            const response = await Axios.post('http://localhost:8000/api/customers', customer);
            console.log(response.data);
        } catch (error) {
            console.log(error.response.data.violations);
            if(error.response.data.violations) {
                const apiErrors= {};
                error.response.data.violations.forEach(violation =>{
                    apiErrors[violation.propertyPath] = violation.message;
                });

                console.log(apiErrors);
            }
        }
    };

    const [errors, setErrors] = useState({
        lastName: "",
        firstName: "",
        email: "",
        company: ""
    });

    return (
        <>
            <h1> Création d'un compte client </h1>
            <form onSubmit={handleSubmit}>
                <Field name="lastName"
                       label="Nom de famille"
                       placeholder="Nom de famille du client"
                       value={customer.name}
                       onChange={handleChange}
                       error={errors.lastName}/>
                <Field name="firstName"
                       label="Prénom"
                       placeholder="Prénom du client"
                       value={customer.firstName}
                       onChange={handleChange}
                       error={errors.firstName}/>
                <Field name="email"
                       label="Email"
                       placeholder="Email du client"
                       value={customer.email}
                       onChange={handleChange}
                       error={errors.email}/>
                <Field name="company"
                       label="Entreprise"
                       placeholder="Entreprise du client"
                       value={customer.company}
                       onChange={handleChange}
                       error={errors.company}/>
                <div className="form-group">
                    <button type="submit" className="btn btn-success"> Enregistrer</button>
                    <Link to="/customers/" className="btn btn-link"> Retour à la liste des clients </Link>
                </div>
            </form>
        </>
    );
};

export default CustomerPage;