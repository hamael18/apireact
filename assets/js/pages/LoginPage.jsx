import React, {useState, useContext} from "react";
import authAPI from "../services/authAPI";
import authContext from "../context/authContext";
import Field from "../components/forms/Fields";

const LoginPage = ({history}) => {

    const {setIsAuthenticated} = useContext(authContext);
    const [credentials, setCredentials] = useState({
        username: "",
        password: ""
    });
    const [error, setError] = useState("");

    const handleChange = ({currentTarget}) => {
        const {value, name} = currentTarget;

        setCredentials({...credentials, [name]: value});
    };

    const handleSubmit = async (event) => {
        event.preventDefault();

        try {
            await authAPI.authenticate(credentials);
            setError("");
            setIsAuthenticated(true);
            history.replace("/customers");
        } catch (error) {
            setError("Aucun compte ne possède cet adresse ou alors les informations ne correspondent pas");
        }
    };

    return (<>
        <h1> Connexion à l'application</h1>

        <form onSubmit={handleSubmit}>
            <Field label="Adresse email" name="username" value={credentials.username} onChange={handleChange}
                   placeholder="Adresse email de connexion" error={error}/>
            <Field label="Mot de passe" name="password" value={credentials.password} onChange={handleChange}
                   type="password" error=""/>
            <div className="form-group">
                <button className="btn btn-success">Connexion</button>
            </div>
        </form>
    </>)
};

export default LoginPage;