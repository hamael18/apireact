import Axios from "axios";
import jwtDecode from "jwt-decode";

/**
 *
 * @param credentials
 * @returns {Promise<boolean>}
 */
function authenticate(credentials) {
    return Axios
        .post("http://127.0.0.1:8000/api/login_check", credentials)
        .then(response => response.data.token)
        .then(token => {
            window.localStorage.setItem("authToken", token);
            setAxiosToken(token);

            return true;
        })
}

/**
 *
 */
function logout() {
    window.localStorage.removeItem("authToken");
    delete Axios.defaults.headers['Authorization'];
}

/**
 *
 * @param token
 */
function setAxiosToken(token) {
    Axios.defaults.headers["Authorization"] = "Bearer " + token;

}

/**
 *
 */
function setUp() {
    const token = window.localStorage.getItem("authToken");
    if (token) {
        const {exp: expiration} = jwtDecode(token);
        if ((expiration * 1000) > new Date().getTime) {
            setAxiosToken(token)
        }
    }
}

/**
 * @returns {boolean}
 */
function isAuthenticated() {
    const token = window.localStorage.getItem("authToken");
    if (token) {
        const {exp: expiration} = jwtDecode(token);
        if ((expiration * 1000) > new Date().getTime) {
            return true;
        }
        return false;
    }
    return false;
}

export default {
    authenticate,
    logout,
    setUp,
    isAuthenticated
};