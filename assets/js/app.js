/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */
import React, {useState} from "react";
import ReactDOM from "react-dom";
import Navbar from "./components/Navbar";
import HomePage from "./pages/homepage";
import {HashRouter, Switch, Route, withRouter} from "react-router-dom";
import CustomersPage from "./pages/CustomersPage";
import InvoicesPage from "./pages/InvoicesPage";
import LoginPage from "./pages/LoginPage";
import authAPI from "./services/authAPI";
import authContext from "./context/authContext";
import PrivateRoute from "./components/PrivateRoute";
import CustomerPage from "./pages/CustomerPage";


// any CSS you require will output into a single css file (app.css in this case)
require('../css/app.css');

authAPI.setUp();

const App = () => {
    const [isAuthenticated, setIsAuthenticated] = useState(authAPI.isAuthenticated());

    const NavbarWithRouter = withRouter(Navbar);

    return (
        <authContext.Provider value={{
            isAuthenticated,
            setIsAuthenticated
        }}>
            <HashRouter>
                <NavbarWithRouter />
                <main className="container pt-5">
                    <Switch>
                        <Route path={"/login"} component={LoginPage} />
                        <PrivateRoute path={"/customers/:id"} component={CustomerPage}/>
                        <PrivateRoute path={"/customers"} component={CustomersPage}/>
                        <PrivateRoute path={"/invoices"} component={InvoicesPage}/>
                        <Route path="/" component={HomePage}/>
                    </Switch>
                </main>
            </HashRouter>
        </authContext.Provider>
    )
};
const rootElement = document.querySelector('#app');
ReactDOM.render(<App/>, rootElement);