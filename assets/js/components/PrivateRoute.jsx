import React, { useContext } from 'react';
import authContext from "../context/authContext";
import { Route, Redirect } from "react-router-dom";

const PrivateRoute = ({path, component}) => {

    const {isAuthenticated} = useContext(authContext);

    return isAuthenticated ? (
        <Route path={path} component={component}/>
    ) : (
        <Redirect to="/login"/>
    );
};

export default PrivateRoute;